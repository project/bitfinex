<?php

namespace Drupal\bitfinex\Form;

use Drupal\bitfinex\Service\BitfinexPublicServiceInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Bitfinex Settings Form.
 */
class BitfinexSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'bitfinex_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['bitfinex.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('bitfinex.settings');

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#description' => $this->t('The API key of your account on Bitfinex.'),
      '#required' => TRUE,
    ];

    $form['api_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API secret'),
      '#description' => $this->t('The API secret of your account on Bitfinex.'),
      '#required' => TRUE,
    ];

    $form['aff_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Affiliate code'),
      '#default_value' => $config->get('aff_code'),
      '#description' => $this->t('The Affiliate code is optional.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (strlen($form_state->getValue('api_key')) != BitfinexPublicServiceInterface::KEY_LENGTH) {
      $form_state->setError($form['api_key'], $this->t('Wrong API key.'));
    }
    if (strlen($form_state->getValue('api_secret')) != BitfinexPublicServiceInterface::KEY_LENGTH) {
      $form_state->setError($form['api_secret'], $this->t('Wrong API secret'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Remove unnecessary values.
    $form_state->cleanValues();
    foreach ($form_state->getValues() as $key => $value) {
      $this->config('bitfinex.settings')->set($key, $value);
    }
    $this->config('bitfinex.settings')->save();
  }

}
