<?php

namespace Drupal\bitfinex\Service;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\ClientInterface;

/**
 * The Bitfinex Authenticated service.
 */
class BitfinexAuthenticatedService implements BitfinexAuthenticatedServiceInterface {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Construct the Bitfinex service.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The Guzzle HTTP client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $http_client, LoggerChannelFactoryInterface $logger_factory) {
    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function getHeader(string $path, array $body) : array {
    $api_key = $this->configFactory->get('bitfinex.settings')->get('api_key');
    if (empty($api_key)) {
      $this->loggerFactory->get('bitfinex')->error('API key empty');

      return [
        'error' => 'API key empty',
      ];
    }
    $api_secret = $this->configFactory->get('bitfinex.settings')->get('api_secret');
    if (empty($api_secret)) {
      $this->loggerFactory->get('bitfinex')->error('API secret empty');

      return [
        'error' => 'API secret empty',
      ];
    }

    $nonce = (int) round(microtime(TRUE) * 1000) * 1000;
    $json_body = Json::encode($body);
    $signature = "/api/{$path}{$nonce}{$json_body}";
    $sig = hash_hmac('sha384', $signature, $api_secret);

    return [
      'Content-Type' => 'application/json',
      'bfx-nonce' => $nonce,
      'bfx-apikey' => $api_key,
      'bfx-signature' => $sig,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function updateMeta(array $body) : array {
    $meta = [];
    if (!empty($this->configFactory->get('bitfinex.settings')->get('aff_code'))) {
      $meta['aff_code'] = $this->configFactory->get('bitfinex.settings')->get('aff_code');
    }
    $body['meta'] = $meta;

    return $body;
  }

  /**
   * {@inheritdoc}
   */
  public function post(string $path, array $body) : array {
    $body = $this->updateMeta($body);
    try {
      $response = $this->httpClient->request(
        'POST',
        (self::AUTHENTICATED_ENDPOINTS . $path),
        ['headers' => $this->getHeader($path, $body), 'json' => $body]
      );
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('bitfinex')->error(
        'Unable to make a request. Code: %code, error: %message',
        ['%code' => $e->getCode(), '%message' => $e->getMessage()]);

      return [
        'code' => $e->getCode(),
      ];
    }

    return Json::decode((string) $response->getBody());
  }

  /**
   * {@inheritdoc}
   */
  public function getDepositAddress(string $wallet, string $method, int $op_renew = 0) : array {
    $body = [
      'wallet' => $wallet,
      'method' => $method,
      'op_renew' => $op_renew == 1 ? 1 : 0,
    ];

    return $this->post('v2/auth/w/deposit/address', $body);
  }

  /**
   * {@inheritdoc}
   */
  public function wallets() : array {
    return $this->post('v2/auth/r/wallets', []);
  }

  /**
   * {@inheritdoc}
   */
  public function movements(string $currency, $start = NULL, $end = NULL, $limit = NULL) : array {
    $body = [];
    if ($start) {
      $body['start'] = $start;
    }
    if ($end) {
      $body['end'] = $end;
    }
    if ($limit) {
      $body['limit'] = $limit;
    }

    return $this->post("v2/auth/r/movements/{$currency}/hist", $body);
  }

}
