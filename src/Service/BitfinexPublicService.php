<?php

namespace Drupal\bitfinex\Service;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\ClientInterface;

/**
 * The Bitfinex Public service.
 */
class BitfinexPublicService implements BitfinexPublicServiceInterface {

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Construct the Bitfinex service.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The Guzzle HTTP client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct(ClientInterface $http_client, LoggerChannelFactoryInterface $logger_factory) {
    $this->httpClient = $http_client;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function platformStatus() : array {
    try {
      $response = $this->httpClient->request('GET', (self::PUBLIC_ENDPOINTS . 'v2/platform/status'));
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('bitfinex')->error(
        'Unable to make a request. Code: %code, error: %message',
        ['%code' => $e->getCode(), '%message' => $e->getMessage()]);

      return [
        'code' => $e->getCode(),
      ];
    }

    return Json::decode((string) $response->getBody());
  }

  /**
   * {@inheritdoc}
   */
  public function getTickers(array $symbols) : array {
    $params = in_array('ALL', $symbols) ? 'ALL' : implode(',', $symbols);
    $url = self::PUBLIC_ENDPOINTS . 'v2/tickers?symbols=' . $params;
    try {
      $response = $this->httpClient->request('GET', $url);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('bitfinex')->error(
        'Unable to make a request. Code: %code, error: %message',
        ['%code' => $e->getCode(), '%message' => $e->getMessage()]);

      return [
        'code' => $e->getCode(),
      ];
    }

    return Json::decode((string) $response->getBody());
  }

  /**
   * {@inheritdoc}
   */
  public function getTicker(string $symbol) : array {
    $url = self::PUBLIC_ENDPOINTS . 'v2/ticker/' . $symbol;
    try {
      $response = $this->httpClient->request('GET', $url);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('bitfinex')->error(
        'Unable to make a request. Code: %code, error: %message',
        ['%code' => $e->getCode(), '%message' => $e->getMessage()]);

      return [
        'code' => $e->getCode(),
      ];
    }

    return Json::decode((string) $response->getBody());
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigs(string $action, string $object, string $detail) : array {
    $url = self::PUBLIC_ENDPOINTS . "v2/conf/pub:{$action}:{$object}:{$detail}";

    try {
      $response = $this->httpClient->request('GET', $url);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('bitfinex')->error(
        'Unable to make a request. Code: %code, error: %message',
        ['%code' => $e->getCode(), '%message' => $e->getMessage()]);

      return [
        'code' => $e->getCode(),
      ];
    }

    return Json::decode((string) $response->getBody());
  }

  /**
   * {@inheritdoc}
   */
  public function foreignExchangeRate(string $first_currency, string $second_currency) : array {
    $body = [
      'ccy1' => $first_currency,
      'ccy2' => $second_currency,
    ];
    try {
      $response = $this->httpClient->request('POST', (self::PUBLIC_ENDPOINTS . 'v2/calc/fx'), ['json' => $body]);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('bitfinex')->error(
        'Unable to make a request. Code: %code, error: %message',
        ['%code' => $e->getCode(), '%message' => $e->getMessage()]);

      return [
        'code' => $e->getCode(),
      ];
    }

    return Json::decode((string) $response->getBody());
  }

}
