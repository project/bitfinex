<?php

namespace Drupal\bitfinex\Service;

/**
 * Implement Bitfinex Public Service Interface.
 */
interface BitfinexPublicServiceInterface {

  /**
   * The url Public endpoint.
   */
  const PUBLIC_ENDPOINTS = 'https://api-pub.bitfinex.com/';

  /**
   * Length of api keys.
   */
  const KEY_LENGTH = 43;

  /**
   * Get Platform Status.
   *
   * Get the current status of the platform, “Operative” or “Maintenance”.
   * Maintenance periods generally last for a few minutes to a couple of hours
   * and may be necessary from time to time during infrastructure upgrades.
   *
   * @return array
   *   Return array data.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function platformStatus() : array;

  /**
   * Get Tickers.
   *
   * The tickers endpoint provides a high level overview of the state of the
   * market. It shows the current best bid and ask, the last traded price, as
   * well as information on the daily volume and price movement over the last
   * day. The endpoint can retrieve multiple tickers with a single query.
   *
   * @param array $symbols
   *   Array symbols cryptocurrency with a prefix.
   *   t - trading, f - funding.
   *   Use "ALL" to get all values.
   *
   * @return array
   *   Return array data.
   *   on trading pairs
   *   [
   *   SYMBOL,
   *   BID,
   *   BID_SIZE,
   *   ASK,
   *   ASK_SIZE,
   *   DAILY_CHANGE,
   *   DAILY_CHANGE_RELATIVE,
   *   LAST_PRICE,
   *   VOLUME,
   *   HIGH,
   *   LOW
   *   ],
   *   on funding currencies
   *   [
   *   SYMBOL,
   *   FRR,
   *   BID,
   *   BID_PERIOD,
   *   BID_SIZE,
   *   ASK,
   *   ASK_PERIOD,
   *   ASK_SIZE,
   *   DAILY_CHANGE,
   *   DAILY_CHANGE_RELATIVE,
   *   LAST_PRICE,
   *   VOLUME,
   *   HIGH,
   *   LOW,
   *   _PLACEHOLDER,
   *   _PLACEHOLDER,
   *   FRR_AMOUNT_AVAILABLE
   *   ],
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getTickers(array $symbols) : array;

  /**
   * Get Ticker.
   *
   * The ticker endpoint provides a high level overview of the state of the
   * market for a specified pair. It shows the current best bid and ask, the
   * last traded price, as well as information on the daily volume and price
   * movement over the last day.
   *
   * @param string $symbol
   *   The symbol you want information about.
   *   (e.g. tBTCUSD, tETHUSD, fUSD, fBTC)
   *
   * @return array
   *   Return array data.
   *   Response Fields:
   *   https://docs.bitfinex.com/reference#rest-public-ticker
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getTicker(string $symbol) : array;

  /**
   * Get Configs.
   *
   * Fetch currency and symbol site configuration data.
   * A variety of types of config data can be fetched by constructing a path
   * with an Action, Object, and conditionally a Detail value.
   * Multiple sets of parameters may be passed, in a single request, to fetch
   * multiple parts of the sites currency and symbol configuration data.
   *
   * @param string $action
   *   Valid action values: : 'map', 'list', 'info', 'fees', 'spec'.
   * @param string $object
   *   Valid object values for the map action: 'currency', 'tx'.
   *   Valid object values for the list action: 'currency', 'pair',
   *   'competitions'.
   *   Valid object values for the info action: 'pair', 'tx'
   *   Valid object values for the fees action: none
   *   Valid object values for the spec action: 'margin'.
   * @param string $detail
   *   The detail parameter is only required for the below action:object values:
   *   A map:currency request requires one of the following detail values:
   *   'sym', 'label', 'unit', 'undl', 'pool', 'explorer'.
   *   A map:tx request requires the following detail value:
   *   'method'.
   *   A list:pair request requires one of the following detail values:
   *   'exchange', 'margin'
   *   A info:tx request requires the following detail value:
   *   'status'.
   *
   * @return array
   *   Return array data.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getConfigs(string $action, string $object, string $detail) : array;

  /**
   * Foreign Exchange Rate. Calculate the exchange rate between two currencies.
   *
   * @param string $first_currency
   *   First currency (base currency).
   * @param string $second_currency
   *   Second currency (quote currency).
   *
   * @return array
   *   Return array data.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function foreignExchangeRate(string $first_currency, string $second_currency) : array;

}
