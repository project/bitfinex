<?php

namespace Drupal\bitfinex\Service;

/**
 * Implement Bitfinex Authenticated Service Interface.
 */
interface BitfinexAuthenticatedServiceInterface {

  /**
   * The url Authenticated endpoint.
   */
  const AUTHENTICATED_ENDPOINTS = 'https://api.bitfinex.com/';

  /**
   * Get header. Get a signature for authenticated requests.
   *
   * @param string $path
   *   Url request.
   * @param array $body
   *   Request field.
   *
   * @return array
   *   Return array data.
   */
  public function getHeader(string $path, array $body): array;

  /**
   * Update meta data for body request.
   *
   * @param array $body
   *   Request field.
   *
   * @return array
   *   Return array body data.
   */
  public function updateMeta(array $body): array;

  /**
   * Post request for API.
   *
   * @param string $path
   *   Request path.
   * @param array $body
   *   Request body field.
   *
   * @return array
   *   Return array data.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function post(string $path, array $body): array;

  /**
   * Get Deposit Address.
   *
   * You must have this function enabled in the Bitfinex API settings.
   *
   * @param string $wallet
   *   Select the wallet from which to transfer (exchange, margin,
   *   funding (can also use the old labels which are exchange, trading and
   *   deposit respectively)).
   * @param string $method
   *   Method of deposit (methods accepted: “bitcoin”, “litecoin”, “ethereum”,
   *   “tetheruso", “tetherusl", “tetherusx", “tetheruss", "ethereumc", "zcash",
   *   "monero", "iota"). For an up-to-date listing of supported currencies
   *   see: https://api-pub.bitfinex.com//v2/conf/pub:map:tx:method.
   * @param int $op_renew
   *   Input 1 to regenerate a wallet address. Defaults to 0 if omitted.
   *
   * @return array
   *   Return array data.
   *   Response Fields:
   *   https://docs.bitfinex.com/reference#rest-auth-deposit-address.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getDepositAddress(string $wallet, string $method, int $op_renew = 0): array;

  /**
   * Get account wallet balances.
   *
   * @return array
   *   Return array data.
   *   Response Fields:
   *   https://docs.bitfinex.com/reference#rest-auth-wallets.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function wallets(): array;

  /**
   * View your past deposits/withdrawals.
   *
   * Currency can be specified to retrieve movements specific to that currency.
   *
   * @param string $currency
   *   Currency (BTC, ...). For an up-to-date listing of supported currencies
   *   see: https://api.bitfinex.com/v2/conf/pub:map:currency:label - Currency
   *   param can be omitted to retrieve recent movements for all currencies.
   * @param mixed $start
   *   Millisecond start time.
   * @param mixed $end
   *   Millisecond end time.
   * @param mixed $limit
   *   Number of records (Max 1000).
   *
   * @return array
   *   Return array data.
   *   Response Fields:
   *   https://docs.bitfinex.com/reference#rest-auth-movements
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function movements(string $currency, $start = NULL, $end = NULL, $limit = NULL) : array;

}
