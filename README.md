
# bitfinex

Original author: [markus_petrux](https://www.drupal.org/user/3391020)


## CONTENTS OF THIS FILE

* INTRODUCTION
* USAGE
* INSTALLATION
* REQUIREMENTS
* CONFIGURATION


## INTRODUCTION

The module is designed to provide services to the Bitfinex Cryptocurrency
Exchange Platform. The module is not a means of payment.

This module only provides services for interacting with the site. In order to
get access you need to register and receive API keys.

To receive the keys, you need to register. After logging in to the Bitfinex,
you can generate keys.


## USAGE

This module is designed to integrate your Drupal with the Bitfinex platform.
It only provides services. To use the module, you need to configure the
access keys on the Bitfinex page /admin/config/development/bitfinex-settings

These permissions will also be available on the standard Bitfinex page at
Administer -> Configuration -> Development -> Bitfinex settings.


## INSTALLATION

1) Copy all contents of this package to your modules directory preserving
   subdirectory structure.

2) Go to Administer -> Modules to install module.


3) Configure API Keys:
   Administer -> Configuration -> Development -> Bitfinex settings


## REQUIREMENTS

This module does not require the installation of other modules.
For authenticated requests, you will need the API keys Bitfineex.


## CONFIGURATION

Configurations for the module are available:

Settings url: /admin/config/development/bitfinex-settings

Or Administer -> Configuration -> Development -> Bitfinex settings
